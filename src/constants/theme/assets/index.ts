// eslint-disable-next-line prettier/prettier
import { ImageSourcePropType } from 'react-native';
import Config from 'react-native-config';
const env = Config.ENV;

type AssetsObjectType = {
  logo?: ImageSourcePropType;
};
type AssetsType = {
  app1: AssetsObjectType;
  app2: AssetsObjectType;
};
const ASSETS: AssetsType = {
  app1: {
    logo: require('../../../assets/app1/logo.png'),
  },
  app2: {
    logo: require('../../../assets/app2/logo.png'),
  },
};
export const THEME_ASSETS = ASSETS[env as keyof AssetsType];
