import Config from 'react-native-config';

const env = Config.ENV;

type ColorsObjectType = {
  primary?: string;
  second?: string;
};
type ColorsType = {
  app1: ColorsObjectType;
  app2: ColorsObjectType;
};

const COLORS: ColorsType = {
  app1: {
    primary: '#ff0000',
    second: '#6fff00',
  },
  app2: {
    primary: '#2900f8',
    second: '#d1f2b9',
  },
};
export const THEME_COLORS = COLORS[env as keyof ColorsType];
