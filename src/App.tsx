/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import { Image, SafeAreaView, StyleSheet, Text } from 'react-native';
import React from 'react';
import Config from 'react-native-config';
import { THEME_COLORS } from './constants/theme/colors';
import { THEME_ASSETS } from './constants/theme/assets';
const App = () => {

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.text}>ENV = {Config.ENV}</Text>
      <Image
        source={THEME_ASSETS.logo}
        style={{
          width: 50,
          height: 50,
        }}
        resizeMode="contain"
      />
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: THEME_COLORS.second,
  },
  text: {
    color: THEME_COLORS.primary,
  },
});
